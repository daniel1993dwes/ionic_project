export const constants = {
  days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  daysOpen: (new Array(7).fill(true)),
  weekday: new Date().getDay()
};
