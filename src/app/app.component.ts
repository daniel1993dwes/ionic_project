import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthServiceService } from './auth/services/auth-service.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  menuDisabled = true;
  public appPages = [
    {
      title: 'New Restaurant',
      url: '/restaurants/add',
      icon: 'restaurant'
    },
    {
      title: 'Restaurants List',
      url: '/restaurants',
      icon: 'list-box'
    },
    {
      title: 'My Account',
      url: '/user/me',
      icon: 'contact'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private AuthService: AuthServiceService,
    private nav: NavController
  ) {
    this.initializeApp();
    this.AuthService.loginChange$.subscribe(
      logged => {
        this.menuDisabled = !logged;
      });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();
    });
  }

  logout() {
    this.AuthService.logout();
    this.nav.navigateRoot(['/auth/login']);
  }
}
