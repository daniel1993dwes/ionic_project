import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {
  token: string;
  authReq: any;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = localStorage.getItem('token');

    if (this.token) {
      this.authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + this.token)});
      return next.handle(this.authReq);
    }
    return next.handle(req);
  }
  constructor() { }
}
