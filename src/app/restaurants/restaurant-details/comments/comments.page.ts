import { Component, OnInit } from '@angular/core';
import { RestaurantServiceService } from '../../services/restaurant-service.service';
import { Restaurant } from '../../interface/restaurant';
import { Comment } from '../../interface/comment';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { CommentModalComponent } from 'src/app/shared/comment-modal/comment-modal.component';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {
  restId: number;
  rest: Restaurant;
  comments: Comment[];
  constructor(
    private RestaurantService: RestaurantServiceService,
    private route: ActivatedRoute,
    private ModalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  async llamarAgregarComentario() {
    const modal = await this.ModalCtrl.create({
      component: CommentModalComponent,
      componentProps: {Id: this.restId}
    });

    await modal.present();

    const result = await modal.onDidDismiss();
    if (result.data !== false) {
      this.comments.push(result.data);
    }
  }

  ionViewWillEnter() {
    this.rest = {
      id: -1,
      name: '',
      image: '',
      cuisine: [],
      description: '',
      phone: '',
      daysOpen: [],
      address: '',
      lat: 0,
      lng: 0,
      stars: '',
      creator: {
        id: -1,
        name: '',
        email: '',
        password: '',
        lat: 0,
        lng: 0,
        avatar: '',
        token: '',
        me: false
      },
      mine: false,
      commented: false,
      distance: 0
    };
    this.restId = this.route.snapshot.parent.parent.params.id;
    this.RestaurantService.getComments(this.restId).subscribe(
      result => {
        this.comments = result;
      }
    );
  }

}
