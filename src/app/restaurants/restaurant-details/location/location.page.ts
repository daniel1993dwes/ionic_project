import { Component, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { RestaurantServiceService } from '../../services/restaurant-service.service';
import { MapComponent } from 'ngx-mapbox-gl';
import { ActivatedRoute } from '@angular/router';
import { Restaurant } from '../../interface/restaurant';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
  rest: Restaurant;
  lat = 0;
  lng = 0;
  zoom = 17;

  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(
    private nav: ActivatedRoute,
    private launchNavigator: LaunchNavigator
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.rest = this.nav.parent.snapshot.parent.data.restaurant;
    this.lat = this.rest.lat;
    this.lng = this.rest.lng;
    this.mapComp.load.subscribe(
      () => {
        this.mapComp.mapInstance.resize(); // Necessary for full height
      }
    );
  }

  async openNavigator() {
    const options: LaunchNavigatorOptions = {};
    await this.launchNavigator.navigate([this.lat, this.lng], options);
  }

}
