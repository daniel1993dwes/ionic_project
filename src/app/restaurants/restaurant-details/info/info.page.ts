import { Component, OnInit } from '@angular/core';
import { RestaurantServiceService } from '../../services/restaurant-service.service';
import { NavController, AlertController, ToastController, Events } from '@ionic/angular';
import { Restaurant } from '../../interface/restaurant';
import { constants } from '../../../constants/constants';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  rest: Restaurant;
  readonly days = constants.days;
  daysOpen = constants.daysOpen;
  weekDay = constants.weekday;
  constructor(
    private RestaurantService: RestaurantServiceService,
    private nav: NavController,
    private AlertCtrl: AlertController,
    private ToastCtrl: ToastController,
    private event: Events,
  ) { }

  ngOnInit() {
    this.rest = {
      id: -1,
      name: '',
      image: '',
      cuisine: [],
      description: '',
      phone: '',
      daysOpen: [],
      address: '',
      lat: 0,
      lng: 0,
      stars: '',
      creator: {
        id: -1,
        name: '',
        email: '',
        password: '',
        lat: 0,
        lng: 0,
        avatar: '',
        token: '',
        me: false
      },
      mine: false,
      commented: false,
      distance: 0
    };
  }

  ionViewWillEnter() {
    this.event.subscribe('restaurant', restaurant => {
      this.rest = restaurant;
      this.rest.distance = Number(this.rest.distance.toFixed(2));
      this.event.unsubscribe('restaurant');
    });
  }

  editarRestaurante(restaurante) {
    this.RestaurantService.restaurantEdit = restaurante;
    this.nav.navigateRoot(['/restaurants/add']);
  }

  async borrarRestaurante(rest) {
    const alert = await this.AlertCtrl.create({
      header: 'Delete Restaurant',
      subHeader: 'You want to delete a restaurant',
      message: 'Do tou want to delete this restaurant',
      buttons: [{
        text: 'Delete',
        handler: async () => {
         await this.ToastCtrl.create({
              duration: 3000,
              position: 'bottom',
              message: 'User registered!'
            });
            await this.ToastCtrl.dismiss();
          this.nav.navigateRoot(['/restaurants']);
        }
      }, 'Close']
    });
    await alert.present();
  }

}
