import { Component, OnInit, Output } from '@angular/core';
import { NavController, Events } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Restaurant } from '../interface/restaurant';

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.page.html',
  styleUrls: ['./restaurant-details.page.scss'],
})
export class RestaurantDetailsPage implements OnInit {
  rest: Restaurant;

  constructor(
    private route: ActivatedRoute,
    private event: Events,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.rest = this.route.snapshot.data['restaurant'];
    this.event.publish('restaurant', this.rest);
  }

}
