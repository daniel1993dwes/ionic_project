import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantDetailsPage } from './restaurant-details.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantDetailsPage,
    children: [
      { path: 'info', loadChildren: './info/info.module#InfoPageModule' },
      { path: 'comments', loadChildren: './comments/comments.module#CommentsPageModule' },
      { path: 'location', loadChildren: './location/location.module#LocationPageModule' },
      { path: '', pathMatch: 'full', redirectTo: 'info' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RestaurantDetailsPage]
})
export class RestaurantDetailsPageModule {}
