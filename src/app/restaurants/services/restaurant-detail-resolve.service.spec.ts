import { TestBed } from '@angular/core/testing';

import { RestaurantDetailResolveService } from './restaurant-detail-resolve.service';

describe('RestaurantDetailResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestaurantDetailResolveService = TestBed.get(RestaurantDetailResolveService);
    expect(service).toBeTruthy();
  });
});
