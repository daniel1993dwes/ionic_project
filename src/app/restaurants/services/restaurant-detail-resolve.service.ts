import { Injectable } from '@angular/core';
import { Restaurant } from '../interface/restaurant';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RestaurantServiceService } from './restaurant-service.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantDetailResolveService implements Resolve<Restaurant> {

  constructor(private RestaurantService: RestaurantServiceService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Restaurant> {
    return this.RestaurantService.getRestaurant(route.params['id']).pipe(
      catchError(error => {
        this.router.navigate(['/restaurants']);
        return of(null);
      })
      );
  }
}
