import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Restaurant } from '../interface/restaurant';
import { Comment } from '../interface/comment';

@Injectable({
  providedIn: 'root'
})
export class RestaurantServiceService {
restURL = environment.baseUrl;
public restaurantEdit: Restaurant;
  constructor(private http: HttpClient) { }

  getComments(id: number): Observable<Comment[]> {
    return this.http.get<{comments: Comment[]}>('restaurants/' + id + '/comments')
    .pipe(
      map(resp => {
        return resp.comments.map(e => {
           e.user.avatar = this.restURL + '/' + e.user.avatar;
           return e;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Comments. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addComment(id: number, comment): Observable<Comment> {
    return this.http.post<{comment: Comment}>('restaurants/' + id + '/comments', comment)
    .pipe(
      map(resp => {
        return resp.comment;
      }), catchError((error: HttpErrorResponse) =>
        throwError(`Error posting Comment. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<{restaurants: Restaurant[]}>('restaurants')
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurants. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurant(id: number): Observable<Restaurant> {
    return this.http.get<{restaurant: Restaurant}>('restaurants/' + id)
    .pipe(
      map(resp => {
        resp.restaurant.image = this.restURL + '/' + resp.restaurant.image;
        resp.restaurant.creator.avatar = this.restURL + '/' + resp.restaurant.creator.avatar;
        resp.restaurant.daysOpen.map(d => Number(d));
        return resp.restaurant;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurantsMine(): Observable<Restaurant[]> {
    return this.http.get<{restaurants: Restaurant[]}>('restaurants/mine')
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getRestaurantsUser(id: number): Observable<Restaurant[]> {
    return this.http.get<{restaurants: Restaurant[]}>('restaurants/user/' + id)
    .pipe(
      map(resp => {
        return resp.restaurants.map(r => {
          r.image = this.restURL + '/' + r.image;
          return r;
        });
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Restaurant ID. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  addRestaurant(rest): Observable<Restaurant> {
    return this.http.post<{restaurant: Restaurant}>('restaurants', rest).pipe(
      map(
        resp => {
          return resp.restaurant;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error adding Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }

  putRestaurant(id: number, rest): Observable<Restaurant> {
    return this.http.put<{restaurant: Restaurant}>('restaurants/' + id, rest).pipe(
      map(
        resp => {
          return resp.restaurant;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error updating Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }

  deleteRestaurant(id: number): Observable<number> {
    return this.http.delete<{identifier: number}>('restaurants/' + id).pipe(
      map(
        resp => {
          return resp.identifier;
        }), catchError((error: HttpErrorResponse) =>
        throwError(`Error deleting Restaurant. Status ${error.status}. Message: ${error.message}`)
        )
    );
  }
}
