import { Component, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { constants } from '../../constants/constants';
import { ModalController, NavController, Events } from '@ionic/angular';
import { ModalMapComponent } from 'src/app/shared/modal-map/modal-map.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { NgForm } from '@angular/forms';
import { Restaurant } from '../interface/restaurant';

@Component({
  selector: 'app-restaurants-form',
  templateUrl: './restaurants-form.page.html',
  styleUrls: ['./restaurants-form.page.scss'],
})
export class RestaurantsFormPage implements OnInit {
  newRest = {
    id: -1,
    name: '',
    description: '',
    daysOpen: [],
    cuisine: [],
    phone: '',
    address: '',
    lat: 0,
    lng: 0,
    image: ''
  };
  lat: number;
  lng: number;
  restaurant: Restaurant;
  cuisine: string;
  daysOpen = constants.daysOpen;
  readonly days = constants.days;
  @ViewChild('restaurantForm') restForm: NgForm;

  constructor(
    private camera: Camera,
    private ModalCtrl: ModalController,
    private geolocation: Geolocation,
    private RestaurantService: RestaurantServiceService,
    private nav: NavController
  ) { }

  ngOnInit() {
    if (this.RestaurantService.restaurantEdit !== undefined) {
      this.restaurant = this.RestaurantService.restaurantEdit;
      this.newRest = this.restaurant;
      this.cuisine = this.newRest.cuisine.join(',');
    }
    this.getLocation();
  }

  addRest() {
    if (this.restForm.invalid || this.newRest.image === '') { return; }

    if (this.restaurant === undefined) {
      this.newRest.cuisine = this.cuisine.split(',');
      this.newRest.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
      this.RestaurantService.addRestaurant(this.newRest).subscribe(
        result => { this.nav.navigateRoot(['/restaurants']); },
        error => { console.error(error); }
      );
    } else {
      this.restaurant.cuisine = this.cuisine.split(',');
      this.restaurant.image = this.restaurant.image.replace('img\\', 'img/');
      this.restaurant.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
      delete this.restaurant.distance;
      delete this.restaurant.stars;
      delete this.restaurant.mine;
      this.restaurant.lat = + this.restaurant.lat;
      this.restaurant.lng = + this.restaurant.lng;

      this.RestaurantService.putRestaurant(this.restaurant.id, this.restaurant).subscribe(
        result => { this.nav.navigateRoot(['/restaurants']); },
        error => { console.error(error); }
      );
    }
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(
      result => {
        this.lat = result.coords.latitude;
        this.lng = result.coords.longitude;
      }).catch(error => {
        console.error(error);
      });
  }

  async getAddress() {
    const modal = await this.ModalCtrl.create({
      component: ModalMapComponent,
      componentProps: { lat: this.lat, lng: this.lng }
    });

    await modal.present();

    const result = await modal.onDidDismiss();
    this.newRest.address = result.data.address;
    this.newRest.lat = result.data.lat;
    this.newRest.lng = result.data.lng;
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.newRest.image = 'data:image/jpeg;base64,' + imageData;
  }

}
