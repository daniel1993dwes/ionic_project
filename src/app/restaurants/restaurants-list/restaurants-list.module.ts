import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantsListPage } from './restaurants-list.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { RestaurantFilterPipe } from './restaurant-filter.pipe';

const routes: Routes = [
  {
    path: '',
    component: RestaurantsListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    RestaurantsListPage,
    RestaurantFilterPipe
  ]
})
export class RestaurantsListPageModule {}
