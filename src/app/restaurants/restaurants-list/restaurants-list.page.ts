import { Component, AfterViewInit } from '@angular/core';
import { Restaurant } from '../interface/restaurant';
import { constants } from '../../constants/constants';
import { RestaurantServiceService } from '../services/restaurant-service.service';
import { AlertController, PopoverController, NavController } from '@ionic/angular';
import { PopoverFilterComponent } from 'src/app/shared/popover-filter/popover-filter.component';

@Component({
  selector: 'app-restaurants-list',
  templateUrl: './restaurants-list.page.html',
  styleUrls: ['./restaurants-list.page.scss'],
})
export class RestaurantsListPage implements AfterViewInit {
  rests: Restaurant[];
  restsWithoutFilters: Restaurant[];
  readonly days = constants.days;
  daysOpen = constants.daysOpen;
  weekDay = constants.weekday;
  orderByName = false;
  showOpen = false;
  search = '';

  constructor(
    private RestaurantService: RestaurantServiceService,
    private AlertCtrl: AlertController,
    private PopoverCtrl: PopoverController,
    private nav: NavController
  ) { }

  ngAfterViewInit() {
    this.RestaurantService.getRestaurants().subscribe(
      result => {
        this.rests = result;
        this.restsWithoutFilters = this.rests;
        this.rests.map(restaurant => {
      Number(restaurant.distance.toFixed(2));
    });
      }
    );
  }

  filterItems(event) {
    const search = event.target.value;
    this.search = search;
  }

  async showPopover(event) {
    const popover = await this.PopoverCtrl.create({
      component: PopoverFilterComponent,
      event,
      componentProps: {
        orderByName: this.orderByName,
        showOpen: this.showOpen
      }
    });

    await popover.present();
    const result = (await popover.onDidDismiss()).data;
    if (result) {
      if (result.showOpen !== undefined) {
        this.togleShowOpen();
      } else {
        this.togleOrderByName();
      }
    }
  }

  togleShowOpen() {
    this.showOpen = !this.showOpen;
  }

  togleOrderByName() {
    this.orderByName = !this.orderByName;
  }

  async borrarRestaurante(rest) {
    const alert = await this.AlertCtrl.create({
      header: 'Delete Restaurant',
      subHeader: 'You want to delete a restaurant',
      message: 'Do tou want to delete this restaurant',
      buttons: [{
        text: 'Delete',
        handler: () => {
          this.rests.splice(this.rests.indexOf(rest), 1);
        }
      }, 'Close']
    });
    await alert.present();
  }

  editarRestaurante(restaurante) {
    this.RestaurantService.restaurantEdit = restaurante;
    this.nav.navigateRoot(['/restaurants/add']);
  }

}
