import { User } from 'src/app/auth/interface/user';

export interface Restaurant {
  id: number;
  name: string;
  description: string;
  daysOpen: string[];
  phone: string;
  image: string;
  address: string;
  cuisine: string[];
  stars: string;
  lat: number;
  lng: number;
  creator: User;
  mine: boolean;
  commented: boolean;
  distance: number;
}
