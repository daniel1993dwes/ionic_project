import { User } from '../../auth/interface/user';

export interface Comment {
  id: number;
  stars: number;
  restaurantId: number;
  user: User;
  text: string;
  date: Date;
}
