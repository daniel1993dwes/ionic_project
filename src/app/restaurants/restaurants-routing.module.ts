import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantDetailResolveService } from './services/restaurant-detail-resolve.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: './restaurants-list/restaurants-list.module#RestaurantsListPageModule'
  },
  {
    path: 'add',
    loadChildren: './restaurants-form/restaurants-form.module#RestaurantsFormPageModule'
  },
  { path: 'details/:id', loadChildren: './restaurant-details/restaurant-details.module#RestaurantDetailsPageModule',
  resolve: {
    restaurant: RestaurantDetailResolveService
  } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantsRoutingModule { }
