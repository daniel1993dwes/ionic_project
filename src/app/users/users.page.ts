import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../auth/interface/user';
import { MapComponent } from 'ngx-mapbox-gl';
import { ModalController } from '@ionic/angular';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { EditAvatarComponent } from './edit-avatar/edit-avatar.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  zoom = 17;
  lat: number;
  lng: number;
  user: User;

  @ViewChild(MapComponent) MapComp: MapComponent;

  constructor(
    private route: ActivatedRoute,
    private ModalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.user = this.route.snapshot.data.user;
    this.lat = this.user.lat;
    this.lng = this.user.lng;
    this.MapComp.load.subscribe(
      () => {
        this.MapComp.mapInstance.resize();
      }
    )
  }

  async changeProfile() {
    const modal = await this.ModalCtrl.create({
      component: EditProfileComponent
    });

    await modal.present();

    const result = await modal.onDidDismiss();

    if (result.data !== false) {
      this.user.name = result.data.name;
      this.user.email = result.data.email;
    }
  }

  async changeAvatar() {
    const modal = await this.ModalCtrl.create({
      component: EditAvatarComponent
    });

    await modal.present();

    const result = await modal.onDidDismiss();

    if (result.data !== false) {
      this.user.avatar = result.data.avatar;
    }
  }
}
