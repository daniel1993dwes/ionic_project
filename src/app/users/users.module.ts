import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UsersPage } from './users.page';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { UserResolverService } from './services/user-resolver.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { SharedModule } from '../shared/shared.module';
import { EditAvatarComponent } from './edit-avatar/edit-avatar.component';

const routes: Routes = [
  {
    path: 'me',
    component: UsersPage,
    resolve: {
      user: UserResolverService
    }
  },
  {
    path: ':id',
    component: UsersPage,
    resolve: {
      user: UserResolverService
    }
  }
];

@NgModule({
  entryComponents: [
    EditProfileComponent,
    EditAvatarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxMapboxGLModule,
    SharedModule
  ],
  declarations: [
    UsersPage,
    EditProfileComponent,
    EditAvatarComponent
  ]
})
export class UsersPageModule {}
