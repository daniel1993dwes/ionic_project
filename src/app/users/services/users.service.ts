import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { User } from 'src/app/auth/interface/user';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  restURL = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getUser(id: number): Observable<User> {
      return this.http.get<{user: User}>('users/' + id)
    .pipe(
      map(resp => {
        resp.user.avatar = this.restURL + '/' + resp.user.avatar;
        return resp.user;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting Users by Id. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  getUserMe(): Observable<User> {
      return this.http.get<{user: User}>('users/me')
    .pipe(
      map(resp => {
        resp.user.avatar = this.restURL + '/' + resp.user.avatar;
        return resp.user;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error getting User Me. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  putUserMe(user): Observable<User> {
      return this.http.put<{user: User}>('users/me', user)
    .pipe(
      map(resp => {
        return resp.user;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error putting Name and Email. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  putUserMeAvatar(user): Observable<User> {
      return this.http.put<{user: User}>('users/me/avatar', {avatar: user})
    .pipe(
      map(resp => {
        resp.user.avatar = this.restURL + '/' + resp.user.avatar;
        return resp.user;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error putting Avatar. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }

  putUserMePassword(user): Observable<User> {
      return this.http.put<{user: User}>('users/me/password', user)
    .pipe(
      map(resp => {
        return resp.user;
      }), catchError((error: HttpErrorResponse) =>
      throwError(`Error putting Password. Status ${error.status}. Message: ${error.message}`)
      )
    );
  }
}
