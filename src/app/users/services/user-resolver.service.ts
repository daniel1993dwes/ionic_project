import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { User } from 'src/app/auth/interface/user';
import { UsersService } from './users.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {

  constructor(private UserService: UsersService,
    private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<User> {
      const id = route.params['id'];
      if (!isNaN(id)) {
        return this.UserService.getUser(+id).pipe(
          catchError(error => {
            this.router.navigate(['/restaurants']);
            return of(null);
        })
      );
      } else {
        return this.UserService.getUserMe().pipe(
          catchError(error => {
            return of(null);
        })
      );
      }

    }
}
