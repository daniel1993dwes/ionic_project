import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  user = {
    name: '',
    email: ''
  };
  password1 = '';
  password2 = '';

  errorName = false;
  errorPassword = false;

  constructor(
    private UserService: UsersService,
    private ModalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  editProfile() {
    this.UserService.putUserMe(this.user).subscribe(
      result => {
        this.ModalCtrl.dismiss({
          name: this.user.name,
          email: this.user.email
        });
      },
      error => {
        this.errorName = true;
      }
    );
  }

  endModal() {
    this.ModalCtrl.dismiss(false);
  }

  editPassword() {
    this.UserService.putUserMePassword({password: this.password1}).subscribe(
      result => {
        this.ModalCtrl.dismiss(true);
      },
      error => {
        this.errorPassword = true;
      }
    );
  }

}
