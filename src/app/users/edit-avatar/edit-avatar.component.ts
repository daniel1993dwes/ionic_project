import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-edit-avatar',
  templateUrl: './edit-avatar.component.html',
  styleUrls: ['./edit-avatar.component.scss']
})
export class EditAvatarComponent implements OnInit {
  user = {
    avatar: ''
  };
  avatarBoolean = false;
  errorAvatar = false;

  constructor(
    private ModalCtrl: ModalController,
    private camera: Camera,
    private UserService: UsersService
  ) { }

  ngOnInit() {
  }

  addPhoto() {
    this.UserService.putUserMeAvatar(this.user.avatar).subscribe(
      result => {});
      this.ModalCtrl.dismiss({avatar: this.user.avatar});
  }

  endModal() {
    this.ModalCtrl.dismiss(false);
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.user.avatar = 'data:image/jpeg;base64,' + imageData;

    if (this.user.avatar !== '' || undefined) {
      this.avatarBoolean = true;
    }
  }
}
