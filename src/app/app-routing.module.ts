import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActiveGuard } from './guards/logout-active.guard';
import { LoginActiveGuard } from './guards/login-active.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule', canActivate: [LogoutActiveGuard]
  },
  {
    path: 'restaurants',
    loadChildren: './restaurants/restaurants.module#RestaurantsModule', canActivate: [LoginActiveGuard]
  },
  { path: 'user', loadChildren: './users/users.module#UsersPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
