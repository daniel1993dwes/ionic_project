import { TestBed, async, inject } from '@angular/core/testing';

import { CanDeactivateFormGuard } from './can-deactivate.guard';

describe('CanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanDeactivateFormGuard]
    });
  });

  it('should ...', inject([CanDeactivateFormGuard], (guard: CanDeactivateFormGuard) => {
    expect(guard).toBeTruthy();
  }));
});
