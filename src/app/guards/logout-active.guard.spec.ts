import { TestBed, async, inject } from '@angular/core/testing';

import { LogoutActiveGuard } from './logout-active.guard';

describe('LogoutActiveGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogoutActiveGuard]
    });
  });

  it('should ...', inject([LogoutActiveGuard], (guard: LogoutActiveGuard) => {
    expect(guard).toBeTruthy();
  }));
});
