import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { MatchDirective } from './validators/match.directive';
import { OneCheckedDirective } from './validators/one-checked.directive';
import { ModalMapComponent } from './modal-map/modal-map.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PopoverFilterComponent } from './popover-filter/popover-filter.component';
import { CommentModalComponent } from './comment-modal/comment-modal.component';

@NgModule({
  declarations: [
    StarRatingComponent,
    MatchDirective,
    OneCheckedDirective,
    ModalMapComponent,
    PopoverFilterComponent,
    CommentModalComponent,
  ],
  entryComponents: [
    ModalMapComponent,
    PopoverFilterComponent,
    CommentModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxMapboxGLModule
  ],
  exports: [
    StarRatingComponent,
    MatchDirective,
    OneCheckedDirective,
    PopoverFilterComponent,
    CommentModalComponent,
  ]
})
export class SharedModule { }
