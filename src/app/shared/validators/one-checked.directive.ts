import { Directive } from '@angular/core';
import { Validator, AbstractControl, FormGroup, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appOneChecked]',
  providers: [{provide: NG_VALIDATORS, useExisting: OneCheckedDirective, multi: true}]
})
export class OneCheckedDirective implements Validator {

  validate(group: AbstractControl): { [key: string]: any } {
    console.log('llega');
    if (group instanceof FormGroup) {
      if (Object.values(group.value).every(v => v === false)) { // No input checked
        return { 'oneChecked': true };
      }
    }

    return null; // No errors
  }

  constructor() { }

}
