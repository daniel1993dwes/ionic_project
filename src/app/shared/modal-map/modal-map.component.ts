import { Component, Input, ViewChild, AfterViewInit } from '@angular/core';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import { ModalController } from '@ionic/angular';
import { MapComponent } from 'ngx-mapbox-gl';


@Component({
  selector: 'app-modal-map',
  templateUrl: './modal-map.component.html',
  styleUrls: ['./modal-map.component.scss']
})
export class ModalMapComponent implements AfterViewInit {
  zoom = 17;
  @Input() lat: number;
  @Input() lng: number;
  address = 'No Street';
  fullscreen = true;

  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(
    public ModalCtrl: ModalController,

  ) { }

  changePosition(result: Result) {
    this.lat = result.geometry.coordinates[1];
    this.lng = result.geometry.coordinates[0];
    this.address = result.place_name;
  }

  ngAfterViewInit() {
    this.mapComp.load.subscribe(
      () => {
        this.mapComp.mapInstance.resize(); // Necessary for full height
      }
    );
    this.zoom = 17;
  }

  endModal() {
    this.ModalCtrl.dismiss({lat: this.lat, lng: this.lng, address: this.address});
  }

}
