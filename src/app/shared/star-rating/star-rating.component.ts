import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {
  @Input() rating: number;
  starsEmpty: number[];
  starsFull: number[];
  maximumStars = 5;
  name = 'star';

  constructor() { }

  ngOnInit() {
    this.starsFull = (new Array(this.rating).fill(1));
    this.starsEmpty = (new Array(this.maximumStars - this.rating).fill(1));

  }
}
