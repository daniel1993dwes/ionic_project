import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RestaurantServiceService } from 'src/app/restaurants/services/restaurant-service.service';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.scss']
})
export class CommentModalComponent implements OnInit {
  @Input() Id: number;
  comment = {
    text: '',
    stars: 0,
  };
  constructor(
    public ModalCtrl: ModalController,
    private RestaurantService: RestaurantServiceService
  ) { }

  ngOnInit() {
  }

  endModal() {
    this.ModalCtrl.dismiss(false);
  }

  addComment() {
    this.RestaurantService.addComment(this.Id, this.comment).subscribe(
      result => {
        this.ModalCtrl.dismiss(this.comment);
      });
  }
}
