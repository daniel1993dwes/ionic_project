import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-filter',
  templateUrl: './popover-filter.component.html',
  styleUrls: ['./popover-filter.component.scss']
})
export class PopoverFilterComponent implements OnInit {
  @Input() orderByName: boolean;
  @Input() showOpen: boolean;

  constructor(
    public PopoverCtrl: PopoverController
  ) { }

  ngOnInit() {
  }

  togleShowOpen(val: boolean) {
    this.PopoverCtrl.dismiss({showOpen: this.showOpen });
  }

  togleOrderName() {
    this.PopoverCtrl.dismiss({orderByName: this.orderByName});

  }

}
