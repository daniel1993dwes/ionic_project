import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { ToastController, NavController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  user = {
    name: '',
    password: '',
    email: '',
    avatar: '',
    lat: 0,
    lng: 0
  };
  password2: string;
  constructor(
    private AuthService: AuthServiceService,
    private toast: ToastController,
    private nav: NavController,
    private camera: Camera,
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
    this.getLocation();
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(
      result => {
        this.user.lat = result.coords.latitude;
        this.user.lng = result.coords.longitude;
      }).catch(error => {
        console.error(error);
      });
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.user.avatar = 'data:image/jpeg;base64,' + imageData;
  }

  register() {
    this.AuthService.register(this.user).subscribe(
      async () => {
        (await this.toast.create({
          duration: 3000,
          position: 'bottom',
          message: 'User registered!'
        })).present();
        this.nav.navigateRoot(['/auth/login']);
      }
    );
  }

}
