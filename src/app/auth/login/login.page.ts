import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user = {
    email: '',
    password: '',
    lat: 0,
    lng: 0
  };
  constructor(
    private AuthService: AuthServiceService,
    private router: Router,
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
    this.getLocation();
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(
      result => {
        this.user.lat = result.coords.latitude;
        this.user.lng = result.coords.longitude;
      }).catch(error => {
        console.error(error);
      });
  }

  login() {
    this.AuthService.login(this.user).subscribe(
      rest => {
        this.router.navigate(['/restaurants']);
      },
      error => {
        console.log(error);
      }
    );
  }

  ionViewWillEnter() {
    this.user = {
      email: '',
      password: '',
      lat: 0,
      lng: 0
    };
    this.getLocation();
  }

}
